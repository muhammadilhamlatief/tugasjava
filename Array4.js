console.log('NO 4')
function dataHandling(arr) {
    let x = [];
    for(a = 0; a < arr.length; a++){
        x.push({
            Nomor : arr[a][0],
            Nama : arr[a][1],
            TTL : arr[a][2]+' '+ arr[a][3],
            Hobi : arr[a][4]
        });
    }
    for (let item of x) {
        console.log(
            `Nomor ID : ${item.Nomor}\nNama : ${item.Nama}\nTTL : ${item.TTL}\nHobi : ${item.Hobi}\n`
        );
    }
}
const input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
dataHandling(input);