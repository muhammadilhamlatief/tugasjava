// No 6 Array

function dataHandling2(arr){
    //teks array 1
    let teksAkhir = arr[1].concat("Elsharawy");
    arr.splice(1, 1, teksAkhir);
    //teks array 2
    let teksAwal = arr[2].split(" ");
    teksAwal.unshift("Provinsi");
    let gabungTeks = teksAwal.join(" ");
    arr.splice(2, 1, gabungTeks);
    //teks array 4
    arr.splice(4, 1, "Pria", "SMA Internasional Metro");
    //modifikasi tanggal bulan tahun
    let spTanggal = arr[3].split("/");
    let sbst = spTanggal[1].substring(1);
    let bulan = Number(sbst);
    switch(bulan) {
        case 1:
            bulan = "Januari";
        break;
        case 2:
            bulan = "Februari";
        break;
        case 3:
            bulan = "Maret";
        break;
        case 4:
            bulan = "April";
        break;
        case 5:
            bulan = "Mei";
        break;
        case 6:
            bulan = "Juni";
        break;
        case 7:
            bulan = "Juli";
        break;
        case 8:
            bulan = "Agustus";
        break;
        case 9:
            bulan = "September";
        break;
        case 10:
            bulan = "Oktober";
        break;
        case 11:
            bulan = "November";
        break;
        case 12:
            bulan = "Desember";
        break;
    }
    // array tanggal
    let arrTanggal = spTanggal[2]+' '+spTanggal[0]+' '+spTanggal[1];
    let splTanggal = arrTanggal.split(' ');
    // gabungkan tanggal
    let gbTanggal = spTanggal.join('-');
    //pembatasan nama 15 karakter
    let slNama = arr[1].slice(0, 14);

    console.log(arr);
    console.log("=================");
    console.log(bulan);
    console.log("=================");
    console.log(splTanggal);
    console.log("=================");
    console.log(gbTanggal);
    console.log("=================");
    console.log(slNama);
}
const input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);